using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShipController : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private Rigidbody2D _shipRb;
    [SerializeField] private float _speed = 5f;
    [SerializeField] private Bullet _missile;
    [SerializeField] private Transform _firePoint;

    private PlayerInput _playerInput;
    private PlayerControls _controls;

    private InputAction _moveInputAction;
    private InputAction _aimInputAction;
    private InputAction _pointInputAction;
    private InputAction _fireInputAction;

    private void Awake()
    {
        _controls = new PlayerControls();

        TryGetComponent(out _playerInput);
        _moveInputAction = _playerInput.actions[_controls.Ship.Move.name];
        _aimInputAction = _playerInput.actions[_controls.Ship.Aim.name];
        _pointInputAction = _playerInput.actions[_controls.Ship.Point.name];
        _fireInputAction = _playerInput.actions[_controls.Ship.Fire.name];
    }

    private void OnEnable()
    {
        _fireInputAction.performed += HandleFire;
    }

    private void OnDisable()
    {
        _fireInputAction.performed -= HandleFire;
    }

    private void FixedUpdate()
    {
        Move();
        Aim();
    }

    private void Move()
    {
        _shipRb.velocity = _speed * _moveInputAction.ReadValue<Vector2>().SnapToDirectionNormalized(8);
    }

    private void Aim()
    {
        Vector2 aimDirection;

        if (_playerInput.currentControlScheme == _controls.MouseScheme.name)
        {
            Vector3 screenPos = _pointInputAction.ReadValue<Vector2>();
            screenPos.z = _shipRb.transform.position.z - _mainCamera.transform.position.z;
            aimDirection = (_mainCamera.ScreenToWorldPoint(screenPos) - _shipRb.transform.position).normalized;
        }
        else
        {
            aimDirection = _aimInputAction.ReadValue<Vector2>().normalized;
        }

        if (aimDirection.magnitude > 0f)
        {
            _shipRb.transform.rotation = Quaternion.LookRotation(Vector3.forward, aimDirection);
        }
    }

    private void HandleFire(InputAction.CallbackContext context)
    {
        Instantiate(_missile, _firePoint.position, _firePoint.rotation);
    }
}
