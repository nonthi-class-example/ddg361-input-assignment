using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtils
{
    public static Vector2 SnapToDirectionNormalized(this Vector2 initialDir, int dirCount)
    {
        if (Mathf.Approximately(initialDir.sqrMagnitude, 0f))
        {
            return Vector2.zero;
        }

        Vector2 result = new Vector2();

        float angle = Mathf.Atan2(initialDir.y, initialDir.x);
        float normalized = Mathf.Repeat(angle / (Mathf.PI * 2f), 1f);
        angle = Mathf.Round(normalized * dirCount) * Mathf.PI * 2f / dirCount;
        result.x = Mathf.Cos(angle);
        result.y = Mathf.Sin(angle);

        return result;
    }
}
